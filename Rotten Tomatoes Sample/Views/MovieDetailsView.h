//
// Created by Oskar Gargas on 16.03.15.
// Copyright (c) 2015 OGLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieDetailsView : UIScrollView

@property(nonatomic, strong) UIImageView *posterImageView;

@property(nonatomic, strong) UILabel *movieTitleLabel;
@property(nonatomic, strong) UILabel *directorLabel;

@property(nonatomic, strong) UILabel *criticsRatingLabel;
@property(nonatomic, strong) UILabel *audienceRatingLabel;
@property(nonatomic, strong) UILabel *criticsRatingDescriptionLabel;
@property(nonatomic, strong) UILabel *audienceRatingDescriptionLabel;

@property(nonatomic, strong) UIButton *openRottenTomatoesWebsite;
@property(nonatomic, strong) UIButton *openIMDbWebsite;

@end
