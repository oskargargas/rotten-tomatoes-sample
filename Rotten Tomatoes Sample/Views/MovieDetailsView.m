//
// Created by Oskar Gargas on 16.03.15.
// Copyright (c) 2015 OGLabs. All rights reserved.
//

#import "MovieDetailsView.h"

@implementation MovieDetailsView {
}

#pragma mark - Lifecycle

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];

        self.posterImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"poster_default"]];

        self.movieTitleLabel                = [UILabel new];
        self.directorLabel                  = [UILabel new];
        self.criticsRatingLabel             = [UILabel new];
        self.audienceRatingLabel            = [UILabel new];
        self.criticsRatingDescriptionLabel  = [UILabel new];
        self.audienceRatingDescriptionLabel = [UILabel new];

        self.movieTitleLabel.font = [UIFont boldSystemFontOfSize:self.movieTitleLabel.font.pointSize];
        self.movieTitleLabel.numberOfLines = 3;
        self.movieTitleLabel.lineBreakMode = NSLineBreakByWordWrapping;

        self.openRottenTomatoesWebsite = [UIButton new];
        self.openIMDbWebsite           = [UIButton new];

        self.openRottenTomatoesWebsite.backgroundColor = [UIColor lightGrayColor];
        self.openIMDbWebsite.backgroundColor           = [UIColor lightGrayColor];

        [self.openRottenTomatoesWebsite setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
        [self.openIMDbWebsite           setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];

        [self addSubview:self.posterImageView];
        [self addSubview:self.movieTitleLabel];
        [self addSubview:self.directorLabel];
        [self addSubview:self.criticsRatingLabel];
        [self addSubview:self.audienceRatingLabel];
        [self addSubview:self.criticsRatingDescriptionLabel];
        [self addSubview:self.audienceRatingDescriptionLabel];
        [self addSubview:self.openRottenTomatoesWebsite];
        [self addSubview:self.openIMDbWebsite];
    }

    return self;
}

#pragma mark - View Lifecycle

- (void)layoutSubviews {
    [super layoutSubviews];

    CGFloat marginX = 10.0f, marginY = 10.0f, viewMargin = 1.0f;

    UIEdgeInsets viewPadding = UIEdgeInsetsMake(viewMargin, viewMargin, viewMargin, viewMargin);
    UIEdgeInsets contentPadding = UIEdgeInsetsMake(marginY - viewMargin, marginX - viewMargin, marginY - viewMargin, marginX - viewMargin);

    CGRect layoutRect = UIEdgeInsetsInsetRect(self.bounds, viewPadding);
    CGRect contentRect = UIEdgeInsetsInsetRect(layoutRect, contentPadding);
    CGPoint cursor = contentRect.origin;

    cursor.y = 0 + marginY;

    self.posterImageView.frame = CGRectMake(cursor.x, cursor.y, 53, 67);
    cursor.x += 53 + marginX;

    CGSize titleLabelSize = [self getSizeForLabel:self.movieTitleLabel inWidth:contentRect.size.width - self.posterImageView.frame.size.width - marginX];
    self.movieTitleLabel.frame = CGRectMake(cursor.x, cursor.y, titleLabelSize.width, titleLabelSize.height);

    CGSize directorLabelSize = [self getSizeForLabel:self.directorLabel inWidth:contentRect.size.width - self.posterImageView.frame.size.width - marginX];
    cursor.y = self.posterImageView.frame.origin.y + self.posterImageView.frame.size.height - directorLabelSize.height;
    self.directorLabel.frame = CGRectMake(cursor.x, cursor.y, directorLabelSize.width, directorLabelSize.height);
    cursor.x = contentRect.origin.x;
    cursor.y = self.posterImageView.frame.origin.y + self.posterImageView.frame.size.height + marginY;

    CGSize criticsRatingDescriptionLabelSize = [self.criticsRatingDescriptionLabel.text sizeWithAttributes:@{NSFontAttributeName : self.criticsRatingDescriptionLabel.font}];
    self.criticsRatingDescriptionLabel.frame = CGRectMake(cursor.x, cursor.y, criticsRatingDescriptionLabelSize.width, criticsRatingDescriptionLabelSize.height);
    cursor.x += criticsRatingDescriptionLabelSize.width + marginX;

    CGSize criticsRatingLabelSize = [self getSizeForLabel:self.criticsRatingLabel inWidth:contentRect.size.width - criticsRatingDescriptionLabelSize.width];
    self.criticsRatingLabel.frame = CGRectMake(cursor.x, cursor.y, criticsRatingLabelSize.width, criticsRatingLabelSize.height);
    cursor.x = contentRect.origin.x;
    cursor.y += criticsRatingLabelSize.height + marginY;

    CGSize audienceRatingDescriptionLabelSize = [self.audienceRatingDescriptionLabel.text sizeWithAttributes:@{NSFontAttributeName : self.audienceRatingDescriptionLabel.font}];
    self.audienceRatingDescriptionLabel.frame = CGRectMake(cursor.x, cursor.y, audienceRatingDescriptionLabelSize.width, audienceRatingDescriptionLabelSize.height);
    cursor.x += audienceRatingDescriptionLabelSize.width + marginX;

    CGSize audienceRatingLabelSize = [self getSizeForLabel:self.audienceRatingLabel inWidth:contentRect.size.width - audienceRatingDescriptionLabelSize.width];
    self.audienceRatingLabel.frame = CGRectMake(cursor.x, cursor.y, audienceRatingLabelSize.width, audienceRatingLabelSize.height);

    cursor.x = layoutRect.origin.x;
    cursor.y = layoutRect.origin.y + layoutRect.size.height - 50;

    float buttonWidth = floorf(layoutRect.size.width / 2.0f) - 1;
    self.openRottenTomatoesWebsite.frame = CGRectMake(cursor.x, cursor.y, buttonWidth, 50);
    cursor.x = layoutRect.origin.x + layoutRect.size.width - buttonWidth;

    self.openIMDbWebsite.frame = CGRectMake(cursor.x, cursor.y, buttonWidth, 50);
}

- (CGSize)getSizeForLabel:(UILabel *)label inWidth:(CGFloat)width {             //TODO: Extract as UILabel category or move to helper class.
    return [label.text boundingRectWithSize:CGSizeMake(width, MAXFLOAT)
                                    options:NSStringDrawingUsesLineFragmentOrigin
                                 attributes:@{NSFontAttributeName : label.font}
                                    context:nil].size;
}

@end
