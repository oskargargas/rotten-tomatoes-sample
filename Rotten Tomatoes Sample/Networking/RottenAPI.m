//
// Created by Oskar Gargas on 16.03.15.
// Copyright (c) 2015 OGLabs. All rights reserved.
//

#import <AFNetworking.h>

#import "RottenAPI.h"
#import "Movie.h"

//static NSString *const rotten_api_key = @"npjsjv4h3ecnpqqvz6hdbbrp";
//static NSString *const rotten_api_key = @"gq4b5gd3f83dsprs56mdzp4c";
static NSString *const rotten_api_key = @"7ewcm8s8hyqkz5grn4nfasdc";

@interface RottenAPI ()

@property(nonatomic, strong) AFHTTPSessionManager *manager;
@property(nonatomic, strong) NSURLSessionDataTask *lastTask;

@end

@implementation RottenAPI {
}

#pragma mark - Lifecycle

+ (instancetype)sharedManager {
    static RottenAPI *sharedManager = nil;

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });

    return sharedManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"http://api.rottentomatoes.com/api/public/v1.0/"]];
    }

    return self;
}

- (void)searchForMovie:(NSString *)title resultsPerPage:(NSInteger)resultsPerPage pageNumber:(NSInteger)pageNumber
       completionBlock:(void (^)(id responseObject, NSError *error))completionBlock {

    [self.lastTask cancel];

    self.lastTask = [self.manager GET:@"movies.json"
                           parameters:@{@"apikey" : rotten_api_key, @"q" : title, @"page_limit" : @(resultsPerPage), @"page" : @(pageNumber)}
                              success:^(NSURLSessionDataTask *task, id responseObject) {
                                  if (completionBlock) completionBlock(responseObject, nil);
                              }
                              failure:^(NSURLSessionDataTask *task, NSError *error) {
                                  if (completionBlock) completionBlock(nil, error);
                              }];
}

- (void)getDetailsForMovie:(Movie *)movie completionBlock:(void (^)(id responseObject, NSError *error))completionBlock {

    [self.lastTask cancel];

    self.lastTask = [self.manager GET:[NSString stringWithFormat:@"movies/%d.json", movie.movieId]
                           parameters:@{@"apikey" : rotten_api_key}
                              success:^(NSURLSessionDataTask *task, id responseObject) {
                                  if (completionBlock) completionBlock(responseObject, nil);
                              }
                              failure:^(NSURLSessionDataTask *task, NSError *error) {
                                  if (completionBlock) completionBlock(nil, error);
                              }];
}

@end
