//
// Created by Oskar Gargas on 16.03.15.
// Copyright (c) 2015 OGLabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Movie;

@interface RottenAPI : NSObject

+ (instancetype)sharedManager;

- (void)searchForMovie:(NSString *)title resultsPerPage:(NSInteger)resultsPerPage pageNumber:(NSInteger)pageNumber completionBlock:(void (^)(id responseObject, NSError *error))completionBlock;
- (void)getDetailsForMovie:(Movie *)movie completionBlock:(void (^)(id responseObject, NSError *error))completionBlock;

@end
