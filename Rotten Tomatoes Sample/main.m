//
//  main.m
//  Rotten Tomatoes Sample
//
//  Created by Oskar Gargas on 14.03.2015.
//  Copyright (c) 2015 OGLabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
