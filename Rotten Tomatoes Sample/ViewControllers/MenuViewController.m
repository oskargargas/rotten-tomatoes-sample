//
// Created by Oskar Gargas on 16.03.15.
// Copyright (c) 2015 OGLabs. All rights reserved.
//

#import "MenuViewController.h"

@implementation MenuViewController {
}

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor redColor];
}

@end
