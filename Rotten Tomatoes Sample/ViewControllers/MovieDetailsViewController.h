//
// Created by Oskar Gargas on 16.03.15.
// Copyright (c) 2015 OGLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Movie;

@interface MovieDetailsViewController : UIViewController

- (instancetype)initWithMovie:(Movie *)movie;

@end
