//
//  MoviesViewController.m
//  Rotten Tomatoes Sample
//
//  Created by Oskar Gargas on 14.03.2015.
//  Copyright (c) 2015 OGLabs. All rights reserved.
//

#import <PKRevealController/PKRevealController.h>
#import <CCBottomRefreshControl/UIScrollView+BottomRefreshControl.h>
#import <UIAlertView+BlocksKit.h>
#import <UIBarButtonItem+BlocksKit.h>
#import <NSArray+BlocksKit.h>
#import <UIImageView+AFNetworking.h>

#import "MoviesViewController.h"

#import "RottenAPI.h"
#import "Movie.h"
#import "MovieDetailsViewController.h"


@interface MoviesViewController () <UISearchBarDelegate>

@property(nonatomic, strong) NSArray /* <Movie *> */ *movies;
@property(nonatomic, strong) UISearchBar *searchBar;

@property(nonatomic, assign) NSInteger totalNumberOfItems;
@property(nonatomic, assign) NSInteger itemsPerPage;
@property(nonatomic, assign) NSInteger lastLoadedPage;
@property(nonatomic, copy) NSString *lastSearchedTitle;

@end

@implementation MoviesViewController

#pragma mark - Lifecycle

- (instancetype)init {
    self = [super init];
    if (self) {
        self.movies = [NSArray array];

        self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 360, 64)];
        self.searchBar.delegate = self;

        self.itemsPerPage = 30;
    }

    return self;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

    self.tableView.tableHeaderView = self.searchBar;

    UIRefreshControl *refreshControl = [UIRefreshControl new];
    [refreshControl addTarget:self action:@selector(loadMoreMoviesData:) forControlEvents:UIControlEventValueChanged];
    self.tableView.bottomRefreshControl = refreshControl;

//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] bk_initWithTitle:@"Menu" style:UIBarButtonItemStylePlain handler:^(id sender) {
//        if (self.revealController.state == PKRevealControllerShowsLeftViewController) {
//            [self.revealController showViewController:self.revealController.frontViewController];
//        } else {
//            [self.revealController showViewController:self.revealController.leftViewController];
//        }
//    }];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] bk_initWithTitle:@"Clear" style:UIBarButtonItemStylePlain handler:^(id sender) {
        [self.searchBar resignFirstResponder];
        self.searchBar.text = @"";
        [self clearMoviesData];
    }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    self.navigationItem.title = @"Search";
    self.revealController.recognizesPanningOnFrontView = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationItem.title = @"Rotten Tomatoes";
    self.revealController.recognizesPanningOnFrontView = NO;    //TODO: Change to YES to enable revealVC.

    [super viewWillAppear:animated];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.movies.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *const cellIdentifier = @"MovieCellIdentifier";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }

    Movie *m = self.movies[(NSUInteger) indexPath.row];

    cell.textLabel.text = m.movieTitle;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Critics: %d%% - %@; Audience: %d%% - %@",
                    m.criticsScore, m.criticsRating, m.audienceScore, m.audienceRating];

    //TODO: Not all movies have critics and/or audience score and rating.

    [cell.imageView setImageWithURL:m.posterURL placeholderImage:[UIImage imageNamed:@"poster_default"]];

    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Movie *m = self.movies[(NSUInteger) indexPath.row];
    [self.navigationController pushViewController:[[MovieDetailsViewController alloc] initWithMovie:m] animated:YES];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:NO animated:YES];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self clearMoviesData];
    self.lastSearchedTitle = searchBar.text;
    [self loadMoviesData];

    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.text = @"";
    [searchBar resignFirstResponder];
}

#pragma mark - Movie loading methods

- (void)clearMoviesData {
    self.totalNumberOfItems = 0;
    self.lastLoadedPage = 0;
    self.movies = [NSArray array];

    [self.tableView reloadData];
}

- (void)loadMoviesData {
    [[RottenAPI sharedManager] searchForMovie:self.lastSearchedTitle resultsPerPage:self.itemsPerPage pageNumber:self.lastLoadedPage + 1 completionBlock:^(id responseObject, NSError *error) {
        if (error && error.code != -999) {
            [UIAlertView bk_showAlertViewWithTitle:@"Some networking error"
                                           message:error.localizedDescription
                                 cancelButtonTitle:@"OK" otherButtonTitles:nil handler:nil];
        }

        if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *response = responseObject;

            self.lastLoadedPage++;

            self.totalNumberOfItems = [response[@"total"] integerValue];

            NSArray *movies = response[@"movies"];
            movies = [movies bk_map:^id(id obj) {
                return [Movie movieFromDictionary:obj];
            }];

            self.movies = [self.movies arrayByAddingObjectsFromArray:movies];

            [self.tableView reloadData];

            NSLog(@"Loaded %lu/%ld movie descriptions.", (unsigned long)self.movies.count, (long)self.totalNumberOfItems);
        }
    }];
}

- (void)loadMoreMoviesData:(UIRefreshControl *)refreshController {
    if (self.lastLoadedPage * self.itemsPerPage < self.totalNumberOfItems) {
        NSLog(@"There are more items to load.");
        [self loadMoviesData];
    } else {
        NSLog(@"There are no more items to load.");
    }

    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [refreshController endRefreshing];
    });
}

@end
