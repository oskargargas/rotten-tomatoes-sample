//
// Created by Oskar Gargas on 16.03.15.
// Copyright (c) 2015 OGLabs. All rights reserved.
//

#import "MovieDetailsViewController.h"
#import "Movie.h"
#import "RottenAPI.h"
#import "UIAlertView+BlocksKit.h"
#import "MovieDetailsView.h"
#import "UIImageView+AFNetworking.h"

@interface MovieDetailsViewController ()

@property(nonatomic, strong) Movie *movie;

@end

@implementation MovieDetailsViewController {
}

#pragma mark - Accessors

- (MovieDetailsView *)movieDetailsView {
    if (self.isViewLoaded) {
        return (MovieDetailsView *) self.view;
    }

    return nil;
}

#pragma mark - Lifecycle

- (instancetype)initWithMovie:(Movie *)movie {
    self = [super init];
    if (self) {
        self.movie = movie;
    }

    return self;
}

#pragma mark - View Lifecycle

- (void)loadView {
    [super loadView];

    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.view = [MovieDetailsView new];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.title = self.movie.movieTitle;

    self.movieDetailsView.criticsRatingDescriptionLabel.text  = @"Critics:";            //TODO: localization.
    self.movieDetailsView.audienceRatingDescriptionLabel.text = @"Audience:";

    [self.movieDetailsView.openRottenTomatoesWebsite setTitle:@"Details" forState:UIControlStateNormal];
    [self.movieDetailsView.openRottenTomatoesWebsite addTarget:self action:@selector(openRottenTomatoesWebsiteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];

    [self.movieDetailsView.openIMDbWebsite setTitle:@"IMDb.com" forState:UIControlStateNormal];
    [self.movieDetailsView.openIMDbWebsite addTarget:self action:@selector(openIMDbWebsiteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];

    [self fillViewWithMovieData];

    [[RottenAPI sharedManager] getDetailsForMovie:self.movie completionBlock:^(id responseObject, NSError *error) {
        if (error && error.code != -999) {
            [UIAlertView bk_showAlertViewWithTitle:@"Some networking error"
                                           message:error.localizedDescription
                                 cancelButtonTitle:@"OK" otherButtonTitles:nil handler:nil];
        }

        if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
            [self.movie updateWithDictionary:responseObject];
            [self fillViewWithMovieData];
        }
    }];
}

- (void)fillViewWithMovieData {
    [self.movieDetailsView.posterImageView setImageWithURL:self.movie.posterURL placeholderImage:[UIImage imageNamed:@"poster_default"]];

    self.movieDetailsView.movieTitleLabel.text = self.movie.movieTitle;
    self.movieDetailsView.directorLabel.text = self.movie.directors.count ? self.movie.directors[0] : @"";

    self.movieDetailsView.criticsRatingLabel.text  = self.movie.criticsScore  != -1 ? [NSString stringWithFormat:@"%d%% - %@", self.movie.criticsScore, self.movie.criticsRating]  : @"Not rated";
    self.movieDetailsView.audienceRatingLabel.text = self.movie.audienceScore != -1 ? [NSString stringWithFormat:@"%d%% - %@", self.movie.audienceScore, self.movie.criticsRating] : @"Not rated";

    [self.movieDetailsView layoutSubviews];
}

#pragma mark - UI Actions

- (void)openRottenTomatoesWebsiteButtonClicked:(UIButton *)sender {
    [[UIApplication sharedApplication] openURL:self.movie.rottenURL];
}

- (void)openIMDbWebsiteButtonClicked:(UIButton *)sender {
    [[UIApplication sharedApplication] openURL:self.movie.imdbURL];
}

@end
