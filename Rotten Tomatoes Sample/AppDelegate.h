//
//  AppDelegate.h
//  Rotten Tomatoes Sample
//
//  Created by Oskar Gargas on 14.03.2015.
//  Copyright (c) 2015 OGLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

