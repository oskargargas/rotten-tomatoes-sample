//
// Created by Oskar Gargas on 16.03.15.
// Copyright (c) 2015 OGLabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Movie : NSObject

@property(nonatomic) NSInteger movieId;
@property(nonatomic, copy) NSString *imdbID;

@property(nonatomic, copy) NSString *movieTitle;
@property(nonatomic, strong) NSArray /* <NSString *> */ *directors;

@property(nonatomic, copy) NSString *theatreReleaseDate;
@property(nonatomic, copy) NSString *dvdReleaseDate;

@property(nonatomic, assign) NSInteger criticsScore;
@property(nonatomic, assign) NSInteger audienceScore;

@property(nonatomic, copy) NSString *criticsRating;
@property(nonatomic, copy) NSString *audienceRating;

@property(nonatomic, strong) NSURL *posterURL;
@property(nonatomic, strong) NSURL *rottenURL;
@property(nonatomic, strong) NSURL *detailsURL;

+ (instancetype)movieFromDictionary:(NSDictionary *)dictionary;

- (BOOL)updateWithDictionary:(NSDictionary *)dictionary;

- (NSURL *)imdbURL;

- (NSString *)description;

@end
