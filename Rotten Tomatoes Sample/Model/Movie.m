//
// Created by Oskar Gargas on 16.03.15.
// Copyright (c) 2015 OGLabs. All rights reserved.
//

#import <KZPropertyMapper.h>
#import <NSArray+BlocksKit.h>

#import "Movie.h"

@implementation Movie {
}

#pragma mark - Lifecycle

+ (instancetype)movieFromDictionary:(NSDictionary *)dictionary {
    Movie *movie = [Movie new];
    [movie updateWithDictionary:dictionary];

    return movie;
}

- (BOOL)updateWithDictionary:(NSDictionary *)dictionary {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [KZPropertyMapper logIgnoredValues:NO];                      //TODO: Move this to some model config file.
    });

    if (self.directors == nil) {
        self.directors = [NSArray array];
    }

    NSArray *directors = dictionary[@"abridged_directors"];
    [directors bk_each:^(NSDictionary *obj) {
        self.directors = [self.directors arrayByAddingObject:obj[@"name"]];
    }];

    return [KZPropertyMapper mapValuesFrom:dictionary toInstance:self usingMapping:@{
            @"id"    : KZProperty(movieId),
            @"alternate_ids" : @{
                    @"imdb" : KZProperty(imdbID)
            },
            @"title" : KZProperty(movieTitle),
            @"release_dates" : @{
                    @"theater" : KZProperty(theatreReleaseDate),
                    @"dvd"     : KZProperty(dvdReleaseDate)
            },
            @"ratings" : @{
                    @"critics_score"   : KZProperty(criticsScore),
                    @"audience_score"  : KZProperty(audienceScore),
                    @"critics_rating"  : KZProperty(criticsRating),
                    @"audience_rating" : KZProperty(audienceRating)
            },
            @"posters" : @{
                    @"profile" : KZBox(URL, posterURL)
            },
            @"links" : @{
                    @"alternate" : KZBox(URL, rottenURL),
                    @"self"      : KZBox(URL, detailsURL)
            }
    }];
}

- (NSString *)description {
    NSMutableString *description = [NSMutableString stringWithFormat:@"<%@: ", NSStringFromClass([self class])];
    [description appendFormat:@"self.movieId=%i", self.movieId];
    [description appendFormat:@", self.imdbID=%@", self.imdbID];
    [description appendFormat:@", self.movieTitle=%@", self.movieTitle];
    [description appendFormat:@", self.directors=%@", self.directors];
    [description appendFormat:@", self.theatreReleaseDate=%@", self.theatreReleaseDate];
    [description appendFormat:@", self.dvdReleaseDate=%@", self.dvdReleaseDate];
    [description appendFormat:@", self.criticsScore=%i", self.criticsScore];
    [description appendFormat:@", self.audienceScore=%i", self.audienceScore];
    [description appendFormat:@", self.criticsRating=%@", self.criticsRating];
    [description appendFormat:@", self.audienceRating=%@", self.audienceRating];
    [description appendFormat:@", self.posterURL=%@", self.posterURL];
    [description appendFormat:@", self.rottenURL=%@", self.rottenURL];
    [description appendFormat:@", self.detailsURL=%@", self.detailsURL];
    [description appendString:@">"];
    return description;
}

#pragma mark - Getters

- (NSURL *)imdbURL {
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://www.imdb.com/title/tt%@/", self.imdbID]];
}

@end
