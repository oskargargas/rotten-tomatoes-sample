Rotten Tomatoes Sample
======================

Example application showing basics of networking, JSON parsing and UI.

Environment
-----------

This project is based on [Cocoapods](http://cocoapods.org/), a Ruby Gem used for dependency management. This implies that you will need Ruby interpreter installed on your Mac besides the usual Xcode setup. The preferred way to install Ruby is via [RVM](http://rvm.io/).

Prerequisites
-------------

* Git
* Ruby
* Xcode

Installation instructions
-------------------------

* Clone project from Git and cd into clone dir
* Install dependencies via Cocoapods
* It's meant to be opened using .xcworkspace as all Pods managed projects are!

```bash
git clone git@bitbucket.org:oskargargas/rotten-tomatoes-sample-app.git
cd rotten-tomatoes-sample-app
bundle install
pod install
open "Rotten Tomatoes Sample.xcworkspace"
```
